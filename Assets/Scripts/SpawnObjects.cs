﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnObjects : MonoBehaviour
{
    public GameObject DeathWall;
    public GameObject Ground;
    public GameObject Player;
    private List<GameObject> GroundClone;
    private List<GameObject> WallClone;
    private float z = 0;
    private int GroundCount = 1;
    private int WallCount = 0;

    void Start()
    {
        GroundClone = new List<GameObject>();
        WallClone = new List<GameObject>();
        GroundClone.Add((GameObject)Instantiate(Ground, new Vector3(0, 0, 15), Quaternion.identity));
        SpawnWall(5);
        StartCoroutine(Spawner());
    }

    IEnumerator Spawner()
    {
        while(true)
        {
            SpawnGround();
            yield return new WaitForSeconds(0.1f);
        }
    }

    void SpawnGround()
    {
        Debug.Log(GroundClone.Count);
        if ((int)Player.transform.position[2] == z)
        {
            for (int i = 0; i < (GroundClone.Count - 1); i++)
            {
                Destroy(GroundClone[i]);
            }
            z = 50 * GroundCount;
            GroundClone.Add((GameObject)Instantiate(Ground, new Vector3(0, 0, 15 + z), Quaternion.identity));
            GroundCount++;
        }
        if (GroundClone.Count > 1 && (int)Player.transform.position[2] == (z - 20))
        {
            SpawnWall(18);
        }
    }

    void SpawnWall(int distZ)
    {
        while (WallCount < 10)
        {
            int type = Random.Range(1, 4);
            switch(type)
            {
                case 1:
                    DeathWall.transform.localScale = new Vector3(1.5f, 0.6f, 0.2f);
                    WallClone.Add((GameObject)Instantiate(DeathWall, new Vector3(-1.25f, 0.407f, (z + distZ) + (WallCount * 6)), Quaternion.identity));
                    WallClone.Add((GameObject)Instantiate(DeathWall, new Vector3(1.25f, 0.407f, (z + distZ) + (WallCount * 6)), Quaternion.identity));
                    break;

                case 2:
                    DeathWall.transform.localScale = new Vector3(2.5f, 0.6f, 0.2f);
                    WallClone.Add((GameObject)Instantiate(DeathWall, new Vector3(-0.75f, 0.407f, (z + distZ) + (WallCount * 6)), Quaternion.identity));
                    DeathWall.transform.localScale = new Vector3(0.5f, 0.6f, 0.2f);
                    WallClone.Add((GameObject)Instantiate(DeathWall, new Vector3(1.75f, 0.407f, (z + distZ) + (WallCount * 6)), Quaternion.identity));
                    break;

                case 3:
                    DeathWall.transform.localScale = new Vector3(0.5f, 0.6f, 0.2f);
                    WallClone.Add((GameObject)Instantiate(DeathWall, new Vector3(-1.75f, 0.407f, (z + distZ) + (WallCount * 6)), Quaternion.identity));
                    DeathWall.transform.localScale = new Vector3(2.5f, 0.6f, 0.2f);
                    WallClone.Add((GameObject)Instantiate(DeathWall, new Vector3(0.75f, 0.407f, (z + distZ) + (WallCount * 6)), Quaternion.identity));
                    break;
            }
            WallCount++;
        }
        WallCount = 0;
    }
}
