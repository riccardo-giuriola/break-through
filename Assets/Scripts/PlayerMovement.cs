﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public Rigidbody rb;
    public float forwardForce;
    public float sidewaysForce;
    public float jumpForce;

    void FixedUpdate()
    {
        //Debug.Log(rb.velocity);

        rb.AddForce(0, 0, forwardForce * Time.deltaTime);
        
        if(Input.GetKey("d"))
        {
            rb.AddForce(sidewaysForce * Time.deltaTime, 0, 0);
        }
        if (Input.GetKey("a"))
        {
            rb.AddForce(-sidewaysForce * Time.deltaTime, 0, 0);
        }
        if(Input.GetKey("space"))
        {
            if(rb.position.y < 0.3f)
            {
                rb.AddForce(0, jumpForce * Time.deltaTime, 0);
            }
        }

        rb.velocity = Vector3.ClampMagnitude(rb.velocity, 8);
    }
}
