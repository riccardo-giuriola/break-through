﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollision : MonoBehaviour
{
    public PlayerMovement movement;

    void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.tag == "DeathWall")
        {
            movement.enabled = false;
        }
        if (collision.collider.tag == "Wall")
        {
            Destroy(collision.gameObject);
        }
    }
}
