﻿using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    public Transform player;
    public Text scoreText;

    void Update()
    {
        scoreText.text = ((int)(player.position.z + 7.71) / 2).ToString() + " M";
    }
}
